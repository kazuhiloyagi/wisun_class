#!/usr/bin/env python
# coding: UTF-8
#
# echonetlite_smart_energy_meter.py
#
# ECHONET Lite 低圧スマート電力量計クラス EchonetLiteSmartEnergyMeter
#
# sem = Smart Energy Meter
#
# https://github.com/yawatajunk/Wi-SUN_EnergyMeter を参考にリライト
# Copyright(C) 2017 kazuhiloyagi, 2016 pi@blue-black.ink
#

import datetime

class EchonetLiteSmartEnergyMeter(object):
    """ECHONET Lite 低圧スマート電力量計クラス"""
    """Wi-SUN モジュール(ROHM)の UDP送信コマンド (SKSENDTO) の送信データ DATA 部分の構成 """
    """Wi-SUN モジュール(ROHM)の UDP受信コマンド (ERXUDP) の受信データ DATA 部分の構成 """

    # クラスグループコード
    CLS_GRP_CODE = { 'housing':         b'\x02' }   # 住宅・設備関連機器クラスグループ

    # ECHONET Lite サービス ECV コード
    ESV_DICT = {
        'seti_sna':                     b'\x50',    # プロパティ値書き込み要求不可応答 (応答不要)
        'setc_sna':                     b'\x51',    # プロパティ値書き込み要求不可応答 (応答要)
        'get_sna':                      b'\x52',    # プロパティ値読み出し不可応答
        'inf_sna':                      b'\x53',    # プロパティ値通知不可応答
        'setget_sna':                   b'\x5e',    # プロパティ値書き込み・読み出し不可応答
        'seti_req':                     b'\x60',    # プロパティ値書き込み要求 (応答不要)
        'setc_req':                     b'\x61',    # プロパティ値書き込み要求 (応答要)
        'get_req':                      b'\x62',    # プロパティ値読み出し要求
        'inf_req':                      b'\x63',    # プロパティ値通知要求
        'setget_req':                   b'\x6e',    # プロパティ値書き込み・読み出し要求
        'set_res':                      b'\x71',    # プロパティ値書き込み応答
        'get_res':                      b'\x72',    # プロパティ値読み出し応答
        'inf_res':                      b'\x73',    # プロパティ値通知
        'infc_req':                     b'\x74',    # プロパティ値通知 (応答要)
        'infc_res':                     b'\x7a',    # プロパティ値通知応答
        'setget_res':                   b'\x7e'}    # プロパティ値書き込み・読み出し応答

    # クラスコード
    CLS_LVSM_CODE = b'\x88'                         # 低圧スマート電力量メータ

    # EPC 低圧スマート電力量メータ のプロパティ
    LVSM_EPC_DICT = {
        'operation_status':             b'\x80',    # 動作状態
        'epc_coefficient':              b'\xd3',    # 係数
        'digits':                       b'\xd7',    # 積算電力量 有効桁数
        'amount_energy_normal':         b'\xe0',    # 積算電力量計測値 (正方向計測値)
        'unit_amount_energy':           b'\xe1',    # 積算電力量単位 (正方向、逆方向計測値)
        'hist_amount_energy1_norm':     b'\xe2',    # 積算電力量計測値履歴1 (正方向計測値)
        'amount_energy_rev':            b'\xe3',    # 積算電力量計測値 (逆方向計測値)
        'hist_amount_energy1_rev':      b'\xe4',    # 積算電力量計測値履歴1 (逆方向計測値)
        'day_hist_amount_energy1':      b'\xe5',    # 積算履歴収集日1
        'instant_power':                b'\xe7',    # 瞬時電力計測値
        'instant_current':              b'\xe8',    # 瞬時電流計測値
        'recent_amount_energy_norm':    b'\xea',    # 定時積算電力量計測値 (正方向計測値)
        'recent_amount_energy_rev':     b'\xeb',    # 定時積算電力量計測値 (逆方向計測値)
        'hist_amount_energy2':          b'\xec',    # 積算電力量計測値履歴2 (正方向、逆方向計測値)
        'day_hist_amount_energy2':      b'\xed'}    # 積算履歴収集日2

    # ECHONET Lite 電文構成（フレームフォーマット）
    frame_dict = {
        'ehd':  bytes(2),   # ECHONET Lite電文ヘッダ1,2
        'tid':  bytes(2),   # トランザクションID
        'seoj': bytes(3),   # 送信元ECHONET Liteオブジェクト指定
        'deoj': bytes(3),   # 相手先ECHONET Liteオブジェクト指定
        'esv':  bytes(1),   # ECHONET Liteサービスコード
        'opc':  bytes(1),   # 処理プロパティー数
        'ptys': []}         # プロパティ列 (EPC, PDC, EDH)


    def __init__(self):
        """コンストラクタ"""
        self.frame_dict['ehd'] = b'\x10\x81'
        self.frame_dict['tid'] = b'\x00\x00'
        self.frame_dict['seoj'] = b'\x05\xff\x01'              # SEOJ(05FF:コントローラ)
        self.frame_dict['deoj'] = self.CLS_GRP_CODE['housing'] + self.CLS_LVSM_CODE + b'\x01'
        self.frame_dict['esv'] = self.ESV_DICT['get_req']      # ESV(62:プロパティ値読み出し要求)
        self.frame_dict['opc'] = b'\x01'
        self.frame_dict['ptys'].append(self.LVSM_EPC_DICT['operation_status'] + b'\x00')  # EPC(80:動作状態)


    def get_serialized_frame(self):
        """ECHONET Liteフレーム dict形式をbytes形式列に変換する"""

        res = self.frame_dict['ehd'] + self.frame_dict['tid'] + self.frame_dict['seoj'] + self.frame_dict['deoj'] + \
              self.frame_dict['esv'] + self.frame_dict['opc']
        for i in range(len(self.frame_dict['ptys'])):
            res += self.frame_dict['ptys'][i]

        return res


    @staticmethod   # クラスメソッド宣言
    def parse_frame(res):
        """ECHONET Liteフレーム bytes形式列をdict形式に変換する"""

        bt_res = bytes.fromhex(res)
        if len(bt_res) < 12:  # EHD1～OPC:12byte
            return False
        if not bt_res[0:2] == b'\x10\x81':
            return False

        frame = {'ehd': bt_res[0:2],
                 'tid': int.from_bytes(bt_res[2:4], 'big'),
                 'seoj': bt_res[4:7],
                 'deoj': bt_res[7:10],
                 'esv': bt_res[10:11],
                 'opc': int.from_bytes(bt_res[11:12], 'big'),
                 'ptys': []}

        idx = 12
        try:    # ECHONET Liteプロパティを繰り返し取得する
            for i in range(frame['opc']):
                pty = {'epc': bt_res[idx:idx + 1],
                       'pdc': int.from_bytes(bt_res[idx + 1:idx + 2], 'big')}
                pty['edt'] = bt_res[idx+2:idx+2+pty['pdc']]
                frame['ptys'].append(pty)
                idx += 2 + pty['pdc']
        except:
            return False    # フォーマットエラー

        # もしデータに欠損や余分があったら
        if len(bt_res) != idx:
            return False    # フォーマットエラー

        return frame


    @staticmethod   # クラスメソッド宣言
    def parse_datetime(dt_bytes):
        """30分毎の計測値などに付随する日付&時間パーサー
        dt_bytes: bytes型日付&時間 YYYYMMDDhhmmss (7 byte)
        return: datetime.datetime型"""

        year = int.from_bytes(dt_bytes[0:2], 'big')
        month = int.from_bytes(dt_bytes[2:3], 'big')
        day = int.from_bytes(dt_bytes[3:4], 'big')
        hour = int.from_bytes(dt_bytes[4:5], 'big')
        minute = int.from_bytes(dt_bytes[5:6], 'big')
        second = int.from_bytes(dt_bytes[6:7], 'big')

        return datetime.datetime(year, month, day, hour, minute, second)
