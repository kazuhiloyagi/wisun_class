#!/usr/bin/env python
# coding: UTF-8
#
# postgres.py
#
# PostgreSQL 接続用プログラム
#
#
# Copyright(C) 2017 kazuhiloyagi
#

from __future__ import print_function
import psycopg2
import sqlalchemy
from sqlalchemy import create_engine
import datetime

import user_conf


class PostgresSEM(sqlalchemy.engine.Connection):
    """PostgreSQL Connection クラス"""

    CONST_E7 = 0.1;       # ECHONET Lite EPC コード: 瞬時電力計測値 の単位変換要定数
    CONST_EA = 0.1042;    # ECHONET Lite EPC コード: 定時積算電力量計測値 (正方向計測値) の単位変換要定数

    def __init__(self):
        """コンストラクタ"""

        DB_TYPE = 'postgresql'
        DB_DRIVER = 'psycopg2'
        DB_PORT = '5432'
        POOL_SIZE = 20
        TABLENAME = 'ea'
        SQLALCHEMY_DB_URI = '%s+%s://%s:%s@%s:%s/%s' % (DB_TYPE, DB_DRIVER, user_conf.DB_USER, user_conf.DB_PASSWORD, user_conf.DB_HOST, DB_PORT, user_conf.DB_DBNAME)
        try:
            engine = create_engine(SQLALCHEMY_DB_URI, pool_size=POOL_SIZE, max_overflow=0)   # データベース接続する
            self = engine.connect()
        except OSError as msg:
            print('[DB Error]: {}\n'.format(msg))


    def insert_e7(self, ipower):
        """e7テーブルへレコードを登録する"""

        # トランザクションを利用して Statement を実行
        trans = self.begin()
        try:
            rpower = iPower * self.CONST_E7
            # Statement の実行
            self.execute("""INSERT INTO e7(instant_power, real_power) VALUES (%(int)s, %(real)s);""", {'int': ipower, 'real': rpower})
            trans.commit()
        except:
            trans.rollback()
            raise


    def insert_ea(self, ipower, date_time):
        """eaテーブルへレコードを登録する"""

        # トランザクションを利用して Statement を実行
        trans = self.begin()
        try:
            # 差分を取るために最後のレコードを検索する
            result = self.execute("SELECT amount_power, measured_at FROM ea ORDER BY measured_at DESC LIMIT 1")
            for row in result:
                dif_power = ipower - row[0]
                dif_time = (date_time - datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')) / datetime.timedelta(minutes=30)  # 差分を30分単位で数える
                rpower = dif_power / dif_time

            result.close()
            # Statement の実行
            self.execute("""INSERT INTO ea(amount_power, diff_power, measured_at) VALUES (%(int)s, %(real)s, %(datetime)s);""", {'int': ipower, 'real': rpower, 'datetime': date_time})
            self.commit()
        except:
            trans.rollback()
            raise


    def terminate(self):
        """PostgreSQL を切断する"""

        try:
            self.close()
        except OSError as msg:
            print('[DB Error]: {}\n'.format(msg))
