# README #

スマートメーターのBルートからWi-SUNモジュールを通してECHONET Lite の消費電力値を取得してくるプログラムです。
https://github.com/yawatajunk/Wi-SUN_EnergyMeter を参考にリライトさせてもらいました。

## What is this repository for? ##
自宅のスマートメーター無線ネットワーク図 
![Smart Energy Meter B route Network Diagram](network.png)

* 契約している電力会社へBルートサービスの利用申し込みが必要です。
* Wi-SUNモジュールとしてROHM社の BP35C2 を使用しています。
* 消費電力値として  ECHONET Lite の 最新の30分毎の計測時刻における積算電力量(正方向計測値) を取得します。
* 取得した消費電力値をPostgreSQLに保存します。
* Current Version 0.2
* MIT License

## How do I get set up? ##
### Deployment instructions
本プロジェクトをインストールしたい適当なディレクトリで、次の手順でインストールします。
```
$ git clone https://kazuhiloyagi@bitbucket.org/kazuhiloyagi/wisun_class.git
$ cd wisun_class
$ git checkout 0.2
```

### Dependencies
* お使いのOSの Python3 環境
    * pyseral
    * psycopg2
    * sqlalchemy
* PostgreSQL

次に、PostgreSQLの pg_hba.conf ファイルを書き換えて、作業用標準ユーザーでPsycopg2を使っても認証されるようにします。
```
(Linux系OSの場合)
$ su - postgres
bash-4.2$ vi /var/lib/pgsql/9.5/data/pg_hba.conf  (PostgreSQL-9.5の場合)
```
pg_hba.confファイルを編集して、local connectionsのMETHODを ident から trust に書き換えます。
```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
host    all             all             127.0.0.1/32            trust
```
pg_hba.confファイルを保存したら、PostgreSQLを再起動します。


### Database configuration
PostgreSQLのdbnameデータベースにデータ保存用のテーブルを作成しておきます。
```
(Linux系OSの場合)
$ createdb dbname -E UTF8
$ psql dbname
dbname=> CREATE TABLE e7 (
               id SERIAL NOT NULL PRIMARY KEY,
               instant_power INTEGER,
               real_power REAL,
               measured_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
             );
dbname=> CREATE TABLE ea (
               id SERIAL NOT NULL PRIMARY KEY,
               amount_power INTEGER,
               diff_power REAL,
               measured_at TIMESTAMP NOT NULL
             );
dbname=> \q
```

### How to run
user_conf.pyを編集し、電力会社から届いたスマートメーターのID及びパスワード、PostgreSQLデータベースに接続するユーザー名とパスワードを設定します。
```
SEM_ROUTEB_ID = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'
SEM_PASSWORD = 'XXXXXXXXXXXX'
SEM_INTERVAL = 3  # 瞬時電力取得間隔[s]
SEM_DURATION = 6  # アクティブスキャンduration (通常は変更の必要なし)

DB_USER = 'dbuser'
DB_PASSWORD = 'dbpassword'
DB_HOST = 'localhost'
DB_DBNAME = 'dbname'
```

プログラムの実行にはPython3のコマンドを実行して下さい。
```
(Linux系OSの場合)
$ cd wisun_class
$ python sem.py
```

## Who do I talk to? ##

* kazuhiloyagi
* kazuhilo.yagi(atmark)gmail.com の(atmark)を@に変えて下さい