#!/usr/bin/env python
# coding: UTF-8
#
# wisun_module.py
#
# Wi-SUN モジュール BP35C2(ROHM) 通信クラス
#
# https://github.com/yawatajunk/Wi-SUN_EnergyMeter を参考にリライト
#  Copyright(C) 2017 kazuhiloyagi, 2016 pi@blue-black.ink
#

from __future__ import print_function

import sys
import threading
import time

import serial


class WiSunModuleBp(threading.Thread):
    """Wi-SUN Module BP35C2(ROHM) 通信クラス"""

    WISUN_UDP_ECHONET_PORT = 3610       # ECHONET UDPポート
    WISUN_UDP_PANA_PORT = 716           # PANA認証 UDPポート
    EHD = '1081'                        # ECHONET Lite電文ヘッダー: \0x10\0x81
    ECV_INF_ = '73'                     # ECHONET Lite サービスコードESV コード: プロパティ値通知（応答要） \0x73 (INF)
    ECV_INFC = '74'                     # ECHONET Lite サービスコードESV コード: プロパティ値通知（応答要） \0x74 (INFC)

    def __init__(self):
        """コンストラクタ"""
        super().__init__()

        self.sem_infc_list = []         # sem.py に渡す CHONET Lite サービスコードESV コード: プロパティ値通知（応答要）\0x74 (INFC) の受信データのリスト

        self.msg_list_queue = []        # その他の受信データ用リスト
        self.term_flag = False          # run()の終了フラグ変数

        self.uart = None                # UART 通信オブジェクト
        self.uart_dev = None            # UART 通信デバイス
        self.uart_baud = 115200         # UART BAUDRATE

        self.search_dict = {                # write()用, UART送信後の受信待ちデータ
            'search_words': [],             # UART送信後の受信待ちデータリスト
            'ignore_intermidiate': False,   # 途中の受信データを無視する
            'found_word_list': [],          # 受け取った受信待ちデータリスト
            'start_time': None,             # UART送信時のtime
            'timeout': 0}                   # 設定タイムアウト時間[s]

        self.msg_list_lock = threading.Lock()   # msg_listの排他制御用
                                                # スレッドの Lock クラスのオブジェクト The class implementing primitive lock objects.


    #  Thread クラスのメソッド run()を ECHONET Lite電文用に拡張
    #  UART受信用ループスレッド
    def run(self):
        print('receive thread run...')  # debug
        while not self.term_flag:
            # 1行読み込み（文字列型が戻る)
            msg = self.read()

            if msg:
                # 1行の受信データを分割してパースする
                msg_dict = self.parse_message(msg)

                if msg_dict['COMMAND'] == 'ERXUDP':
                    if 'DATA' in msg_dict:
                        if msg_dict['DATA'][0:4] == self.EHD:
                            # スマートメーターが自発的に発するプロパティ通知
                            if msg_dict['DATA'][20:22] == self.ECV_INF_:
                                # スレッドロックしてメッセージを受信リストに追加
                                self.enqueue_infc(msg_dict)
                            if msg_dict['DATA'][20:22] == self.ECV_INFC:
                                # スレッドロックしてメッセージを受信リストに追加
                                self.enqueue_infc(msg_dict)

                # write()コマンドに対する応答を待つシーケンス
                if self.search_dict['search_words']:     # サーチ中である
                    # サーチワードを受信した。
                    if isinstance(self.search_dict['search_words'][0], list):
                        search_word = str(self.search_dict['search_words'][0][0])
                        if msg_dict['COMMAND'].startswith(search_word):
                            self.search_dict['found_word_list'].append(msg_dict)  # serch_wordではなく msg_dictを返すこと！
                            self.search_dict['search_words'][0].pop(0)
                            # 最初の要素(ポジション0)の値を取り出して、dictからその要素を削除する

                        elif self.search_dict['ignore_intermidiate']:
                            pass    # 途中の受信データを破棄

                        else:    # サーチワードではなかった
                            # スレッドロックしてメッセージを受信リストに追加
                            self.enqueue_message(msg_dict)

                    # self.search_dict['search_words'][0]が文字列の場合 (通常の場合)
                    elif isinstance(self.search_dict['search_words'][0], str):
                        search_word = str(self.search_dict['search_words'][0])
                        if msg_dict['COMMAND'].startswith(search_word):
                            self.search_dict['found_word_list'].append(msg_dict)  # serch_wordではなく msg_dictを返すこと！
                            self.search_dict['search_words'].pop(0)
                            # 最初の要素(ポジション0)の値を取り出して、dictからその要素を削除する

                        elif self.search_dict['ignore_intermidiate']:
                            pass    # 途中の受信データを破棄

                        else:    # サーチワードではなかった
                            # スレッドロックしてメッセージを受信リストに追加
                            self.enqueue_message(msg_dict)

                else:   # サーチ中ではない
                    # スレッドロックしてメッセージを受信リストに追加
                    self.enqueue_message(msg_dict)

            elif self.search_dict['timeout']:
                # もしtimeoutを過ぎていたら初期化する
                if (time.time() - self.search_dict['start_time']) > self.search_dict['timeout']:
                    self.search_dict['found_word_list'] = []
                    self.search_dict['search_words'] = []
                    self.search_dict['timeout'] = 0

            # 他スレッドの処理を待つ
            time.sleep(0.4)


    def terminate(self):
        """run()の停止"""
        self.term_flag = True
        self.join()


    def read(self):
        """1行読み込み（文字列型が戻る)"""
        try:
            res = self.uart.readline().decode().strip()  # Python 3.x uses the UTF-8 encoding by default
            # print('read:' + res)   # debug
            return res
        except OSError as msg:
            print('[Read Error]: {}\n'.format(msg))
            return False


    def write(self, send_msg, search_words=[], ignore=False, timeout=0):
        """UART書き込み & 受信待ち
            send_msg: 送信データ: bytes
            search_word: 受信待ちコマンド
                (例) ['word1', 'word2', ['word31', 'word32']]: 'word1 -> 'word2' -> 'word31' or 'word32'
            ignore: 途中の受信データを無視する
            timeout: タイムアウト時間[s]
        """
        try:
            self.uart.write(send_msg)

            # 送信に対する応答が設定されていたら
            if search_words:
                self.search_dict['found_word_list'] = []
                self.search_dict['ignore_intermidiate'] = ignore
                self.search_dict['start_time'] = time.time()
                self.search_dict['timeout'] = timeout
                # run()で監視しているので、一番最後に設定する
                self.search_dict['search_words'] = search_words
                # run() メソッドで self.search_dict['search_words'] が空になるまで待つ
                while self.search_dict['search_words'] != []:
                    time.sleep(0.005)

                return self.search_dict['found_word_list']

        except OSError as msg:
            print('[Write Error]: {}\n'.format(msg))
            return False


    @staticmethod  # クラスメソッド宣言
    def parse_message(msg):
        """1行の受信データを分割してパースする"""

        msg_dict = {}     # これが戻り値

        # SKSCAN (ACTIVESCAN) 送信に対する応答
        if msg.startswith('Channel Page'):                 # 発見した PAN のチャネルページ
            msg_dict['COMMAND'] = 'ACTIVESCAN'
            cols = msg.split(':')
            msg_dict['Channel Page'] = int(cols[1], base=16) # UINT8
            return msg_dict

        if msg.startswith('Channel'):                      # 発見した PAN の周波数(論理チャネル番号)
            msg_dict['COMMAND'] = 'ACTIVESCAN'
            cols = msg.split(':')
            msg_dict['Channel'] = int(cols[1], base=16)    # UINT8
            return msg_dict

        if msg.startswith('Pan ID'):                       # 発見した PAN の PAN ID
            msg_dict['COMMAND'] = 'ACTIVESCAN'
            cols = msg.split(':')
            msg_dict['Pan ID'] = int(cols[1], base=16)     # UINT16
            return msg_dict

        if msg.startswith('Addr'):                         # アクティブスキャン応答元のMACアドレス
            msg_dict['COMMAND'] = 'ACTIVESCAN'
            cols = msg.split(':')
            msg_dict['Addr'] = cols[1]                     # CHAR * 8バイト
            return msg_dict

        if msg.startswith('LQI'):                          # 受信したビーコンの受信 ED 値(RSSI)
            msg_dict['COMMAND'] = 'ACTIVESCAN'
            cols = msg.split(':')
            msg_dict['LQI'] = int(cols[1], base=16)        # UINT8
            return msg_dict

        if msg.startswith('PairID'):                       # 相手から受信した Pairing ID
            msg_dict['COMMAND'] = 'ACTIVESCAN'
            cols = msg.split(':')
            msg_dict['PairID'] = cols[1]                   # CHAR * 8バイト
            return msg_dict

        cols = msg.split()
        # for col in cols:   # debug
        #     print(col)

        # コマンド送信に対する ROHM BP35C2 からの応答
        if cols[0] == 'OK':
            msg_dict['COMMAND'] = cols[0]
            if len(cols) > 1:
                msg_dict['MESSAGE'] = cols[1:len(cols)]    # もし'OK'以外の受信データがあれば
            return msg_dict

        # イベント受信コマンド
        # SKSCAN 送信に対する応答
        if cols[0] == 'EVENT':
            # print('EVENT col length : ' + str(len(cols)))  # debug
            msg_dict['COMMAND'] = cols[0] + ' ' + cols[1]  # 'EVENT 数字'で保存
            # 0x20: Beacon を受信した
            # 0x21: UDP 送信処理が完了した
            # 0x22: アクティブスキャンが完了した
            # 0x23: ポーリングの実行結果通知
            # 0x24: PANA による接続過程でエラーが発生した(接続が完了しなかった)
            # 0x25: PANA による接続が完了した
            # 0x26: 接続相手からセッション終了要求を受信した
            # 0x27: PANA セッションの終了に成功した
            # 0x28: PANA セッションの終了要求に対する応答がなくタイムアウトした (セッションは終了)
            msg_dict['SENDER'] = cols[2]                   # イベントのトリガーとなったメッセージの発信元 IPv6 アドレス  UINT8 * 16バイト
            if len(cols) == 4:
                msg_dict['PARAM'] = cols[3]                # イベント固有の引数               UINT8
                                                           # EVENT 0x21 の場合:  0: UDP送信成功  1: UDP送信失敗
            return msg_dict

        # UDP受信コマンド
        # SKSENDTO 送信に対する応答だけではなく、Smart Energy Meterからの自発的ステータス送信もこのコマンドで送られてくる
        if cols[0] == 'ERXUDP':
            # print('ERXUDP col length : ' + str(len(cols)))  # debug
            msg_dict['COMMAND'] = cols[0]                  # 'ERXUDP'
            msg_dict['SENDER'] = cols[1]                   # 送信元 IPv6 アドレス            UINT8 * 16バイト
            msg_dict['DEST'] = cols[2]                     # 送信先 IPv6 アドレス            UINT8 * 16バイト
            msg_dict['RPORT'] = int(cols[3], base=16)      # 送信元 ポート番号               UINT16
            if len(cols) > 5:
                msg_dict['LPORT'] = int(cols[4], base=16)      # 送信先 ポート番号               UINT16
                msg_dict['SENDERLLA'] = cols[5]                # 送信元の MAC 層アドレス(64bit)  UINT8 ＊ 8バイト
                if len(cols) == 9:
                    # msg_dict['SECURED'] = int(cols[6], base=16)  # 1:受信した IP パケットを構成する MAC フレームが暗号化されていた場合
                    msg_dict['SECURED'] = int(cols[6])             # 0:受信した IP パケットを構成する MAC フレームが暗号化されていなかった場合
                    msg_dict['DATALEN'] = int(cols[7], base=16)    # 受信したデータの長さ            UINT16
                    msg_dict['DATA'] = cols[8]                     # 受信したデータ                 CHAR
                if len(cols) == 10:
                    msg_dict['RSSI'] = int(cols[6])                # 受信した UDP を構成する最後の MAC フレームの受信 RSSI レベル
                    # msg_dict['SECURED'] = int(cols[6], base=16)  # 1:受信した IP パケットを構成する MAC フレームが暗号化されていた場合
                    msg_dict['SECURED'] = int(cols[7])             # 0:受信した IP パケットを構成する MAC フレームが暗号化されていなかった場合
                    msg_dict['DATALEN'] = int(cols[8], base=16)    # 受信したデータの長さ            UINT16
                    msg_dict['DATA'] = cols[9]                     # 受信したデータ                 CHAR
            return msg_dict

        # if cols[0] == 'ERXTCP':
        #     msg_dict['COMMAND'] = cols[0]
        #     msg_dict['SENDER'] = cols[1]
        #     msg_dict['RPORT'] = int(cols[2], base=16)
        #     msg_dict['LPORT'] = int(cols[3], base=16)
        #     msg_dict['DATALEN'] = int(cols[4], base=16)
        #     msg_dict['DATA'] = cols[5]
        #     return msg_dict

        # if cols[0] == 'ETCP':
        #     msg_dict['COMMAND'] = cols[0]
        #     msg_dict['STATUS'] = int(cols[1], base=16)
        #     msg_dict['HANDLE'] = int(cols[2], base=16)
        #     if msg_dict['STATUS'] == 1:
        #         msg_dict['IPADDR'] = cols[3]
        #         msg_dict['RPORT'] = int(cols[4], base=16)
        #         msg_dict['LPORT'] = int(cols[5], base=16)
        #     return msg_dict

        # SKSREG 送信に対する受信
        if cols[0] == 'ESREG':
            msg_dict['COMMAND'] = cols[0]
            msg_dict['VAL'] = cols[1]
            return msg_dict

        # ACTIVESCAN を実行して発見した PAN を通知
        # SKSCAN (ACTIVESCAN) 送信に対する応答
        if cols[0] == 'EPANDESC':
            msg_dict['COMMAND'] = 'EPANDESC'               # この後に Channel, Channel Page, Pan ID, Addr, LQI, Pair ID, の各dictが来る
            return msg_dict

        # ED SCAN の実行結果を、RSSI 値で一覧表示する
        # SKSCAN (ED SCAN) 送信に対する応答
        if cols[0] == 'EEDSCAN':
            msg_dict['COMMAND'] = 'EEDSCAN'
            return msg_dict

        # ローカルエコー停止前のローカルエコー対策: 'SKSREG SFE 0'
        if cols[0] == 'SKSREG':
            msg_dict['COMMAND'] = 'SKSREG'
            msg_dict['REG'] = cols[1]
            msg_dict['VAL'] = cols[2]
            return msg_dict

        # エラーコード受信
        if cols[0] == 'ER04':                              # 指定されたコマンドがサポートされていない
            msg_dict['COMMAND'] = 'ER04'
            return msg_dict

        # エラーコード受信
        if cols[0] == 'ER05':                              # 指定されたコマンドの引数の数が正しくない
            msg_dict['COMMAND'] = 'ER05'
            return msg_dict

        # エラーコード受信
        if cols[0] == 'ER06':                              # 指定されたコマンドの引数形式や値域が正しくない
            msg_dict['COMMAND'] = 'ER06'
            return msg_dict

        # エラーコード受信
        if cols[0] == 'ER09':                              # UART 入力エラーが発生した
            msg_dict['COMMAND'] = 'ER09'
            return msg_dict

        # エラーコード受信
        if cols[0] == 'ER10':                              # 指定されたコマンドは受付けたが、実行結果が失敗した
            msg_dict['COMMAND'] = 'ER10'
            return msg_dict

        # その他
        # SKLL64 送信に対する IPv6 リンクローカルアドレス受信
        msg_dict['COMMAND'] = 'UNKNOWN'  # unknown message
        msg_dict['MESSAGE'] = cols
        return msg_dict


    def enqueue_infc(self, msg_list):
        """スレッドロックしてプロパティ値通知（応答要）データを受信リストに追加"""
        self.msg_list_lock.acquire()
        self.sem_infc_list.append(msg_list)
        self.msg_list_lock.release()


    def enqueue_message(self, msg_list):
        """スレッドロックしてメッセージを受信リストに追加"""
        self.msg_list_lock.acquire()
        self.msg_list_queue.append(msg_list)
        self.msg_list_lock.release()


    def uart_open(self, dev, baud, timeout):
        """UART接続をオープンする"""
        try:
            # シリアルポート初期化
            self.uart = serial.Serial(dev, baud, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=timeout)
            self.uart_dev = dev
            self.uart_baud = baud
            return True
        except OSError as msg:
            print('[Error]: {}\n'.format(msg))
            return False


    def uart_close(self):
        """UART接続をクローズする"""
        try:
            self.uart.close()
        except OSError as msg:
            print('[Error]: {}\n'.format(msg))


    def set_echoback_off(self):
        """エコーバックを停止する"""
        self.write(b'SKSREG SFE 0\r\n', ['OK'], ignore=True)


    def set_opt(self, flag):
        """ERXUDP, ERXTCPのフォーマット設定
            flag: True: ASCII
                  False: Binary
        """
        current = self.get_opt()
        # 変更無しの場合はモジュールに書き込まない（FLASHへの書き込み制限10000回以下のため）
        if flag and not current:
            self.write(b'WOPT 01\r\n', ['OK 01'])  # 16進数ASCII表示に設定する
        elif not flag and current:
            self.write(b'WOPT 00\r\n', ['OK 00'])  # Binary表示に設定する
        return True


    def get_opt(self):
        """ERXUDP, ERXTCPのフォーマット取得
            retern True: ASCII
                   False: Binary
             デフォルトは 00 = Binary表示
        """
        res = self.write(b'ROPT\r\n', ['OK'])
        bResult = False
        if res:
            if res[0]['MESSAGE'][0] == '01':
                bResult = True
            else:
                bResult = False
            return bResult


    def set_password(self, password):
        """Bルート認証パスワード設定"""
        length = len(password)
        if length < 1 or length > 32:
            result = False
        else:
            bp = '{:X} {}'.format(length, password).encode()  # Python 3.x uses the UTF-8 encoding by default
                                                              # {:X} means Hex binary numbers formatting
            self.write(b'SKSETPWD ' + bp + b'\r\n', ['OK'])
            result = True
        return result


    def set_routeb_id(self, rbid):
        """Bルート認証ID設定"""
        if len(rbid) != 32:
            result = False
        else:
            self.write(b'SKSETRBID ' + rbid.encode() + b'\r\n', ['OK'])  # Python 3.x uses the UTF-8 encoding by default
            result = True
        return result


    def active_scan(self, duration = 6):
        """アクティブスキャン"""

        bScan_end = False
        channel_list = []
        channel_dict = {}
        bd = '{:X}'.format(duration).encode()  # Python 3.x uses the UTF-8 encoding by default
                                               # {:X} means Hex binary numbers formatting
        self.write(b'SKSCAN 2 FFFFFFFF ' + bd + b' 0\r\n', ['OK'])
        try:
            # スキャン終了までのループ
            while not bScan_end:
                # 受信リスト内のメッセージ数があれば
                if self.get_queue_size():
                    # メッセージを受信リストから取り出す
                    msg_list = self.dequeue_message()

                    # beacon 受信
                    if msg_list['COMMAND'] == 'EVENT 20':
                        pass     # pass 文は何もしません

                    elif msg_list['COMMAND'] == 'EPANDESC':
                        channel_dict = {}

                    elif msg_list['COMMAND'] == 'ACTIVESCAN':
                        if 'Channel Page' in msg_list:
                            channel_dict['Channel Page'] = msg_list['Channel Page']
                        elif 'Channel' in msg_list:
                            channel_dict['Channel'] = msg_list['Channel']
                        elif 'Pan ID' in msg_list:
                            channel_dict['Pan ID'] = msg_list['Pan ID']
                        elif 'Addr' in msg_list:
                            channel_dict['Addr'] = msg_list['Addr']
                        elif 'LQI' in msg_list:
                            channel_dict['LQI'] = msg_list['LQI']
                        elif 'PairID' in msg_list:
                            channel_dict['PairID'] = msg_list['PairID']
                        channel_list.append(channel_dict)

                    elif msg_list['COMMAND'] == 'EVENT 22':
                        # 0x22:アクティブスキャンが完了した
                        # BP35C0/BP35C2 コマンドリファレンスマニュアル (DEV 版)より
                        bScan_end = True
                        break
                else:
                    time.sleep(0.01)
        # active_scan()をCtrl+cで終了したとき
        except KeyboardInterrupt:
            channel_list = False   # スキャンキャンセル

        return channel_list


    def get_queue_size(self):
        """受信リスト内のメッセージ数を返す"""
        return len(self.msg_list_queue)


    def dequeue_message(self):
        """メッセージを受信リストから取り出す"""
        self.msg_list_lock.acquire()
        if self.msg_list_queue:
            result = self.msg_list_queue.pop(0)   # pop(0)は最初の要素(ポジション0)の値を取り出して、listからその要素を削除する
        else:
            result = None
        self.msg_list_lock.release()

        return result


    def set_channel(self, ch):
        """スキャン結果からChannelを設定する"""
        bc = '{:02X}'.format(ch).encode()
        # print(b'SKSREG S2 ' + bc + b'\r\n')  # debug
        self.write(b'SKSREG S2 ' + bc + b'\r\n', ['OK'])


    def set_pan_id(self, pan):
        """スキャン結果からPan IDを設定する"""
        bp = '{:04X}'.format(pan).encode()
        # print(b'SKSREG S3 ' + bp + b'\r\n')   # debug
        self.write(b'SKSREG S3 ' + bp + b'\r\n', ['OK'])


    def get_ip6(self, add):
        """MACアドレス(64bit)をIPV6リンクローカルアドレスに変換する"""

        self.uart.write(b'SKLL64 ' + add.encode() + b'\r\n')

        res = self.read()

        return res


    def start_pac(self, ip6):
        """PANA 接続シーケンスを開始する"""

        print('start_pac ...')
        bConnected = False
        st = time.time()

        self.uart.write(b'SKJOIN ' + ip6.encode() + b'\r\n')
        try:
            while True:
                if not bConnected:
                    sentence = self.read()
                    if sentence:
                        res = self.parse_message(sentence)
                        # print('res=' + str(res))  # debug
                        if res['COMMAND'] == 'EVENT 24':
                            print("[EVENT 24]: PANA disconnected.")
                            bConnected = False
                            break
                        elif res['COMMAND'] == 'EVENT 25':
                            print("[EVENT 25]: PANA connected.")
                            bConnected = True
                            break
                        else:
                            self.enqueue_message(res)
                            bConnected = False
                    else:
                        bConnected = False
                # タイムアウト(6s)
                if (time.time() - st) > 6:
                    break

            return bConnected
        except:     # IndexErrorが発生するときのための暫定処理。要検討
            pass


    #@staticmethod
    def dequeue_infc(self):
        """プロパティ値通知（応答要）データをリストから取り出す"""
        self.msg_list_lock.acquire()

        if self.sem_infc_list:
            result = self.sem_infc_list.pop(0)    # 最初の要素(ポジション0)の値を取り出して、listからその要素を削除する
        else:
            result = None

        self.msg_list_lock.release()

        return result


    def restart_pac(self):
        """PANA 接続シーケンスを再開する"""

        print('restart_pac ...')
        bConnected = False
        st = time.time()

        self.uart.write(b'SKREJOIN\r\n')
        try:
            while True:
                if not bConnected:
                    sentence = self.read()
                    if sentence:
                        res = self.parse_message(sentence)
                        # print('res=' + str(res))  # debug
                        if res['COMMAND'] == 'EVENT 24':
                            print("[EVENT 24]: PANA disconnected.")
                            bConnected = False
                            break
                        elif res['COMMAND'] == 'EVENT 25':
                            print("[EVENT 25]: PANA connected.")
                            bConnected = True
                            break
                        else:
                            self.enqueue_message(res)
                            bConnected = False
                    else:
                        bConnected = False
                # タイムアウト(6s)
                if (time.time() - st) > 6:
                    break

            return bConnected
        except:     # IndexErrorが発生するときのための暫定処理。要検討
            pass


    def udp_send(self, handle, ip6, security, port, message):
        """UDPでコマンドを送信する"""
        if security:
            sec_bt = b' 1'
        else:
            sec_bt = b' 0'
        len_bt = ' {:04X} '.format(len(message)).encode()
        port_bt = ' {:04X}'.format(port).encode()

        st = time.time()
        print('udp_send ...')
        bSended = False
        self.write(b'SKSENDTO ' + str(handle).encode() + b' ' + ip6.encode() + port_bt + sec_bt + len_bt + message + b'\r\n', ['EVENT 21'])
        try:
            while True:
                if not bSended:
                    sentence = self.read()
                    if sentence:
                        res = self.parse_message(sentence)
                        if res['PARAM'] == '01':
                            print('[Error]: UDP transmission.\n')
                            bSended = False
                            break
                        else:
                            rint('UDP transmission succeed...\n')
                            self.enqueue_message(res)
                            bSended = True     # 送信成功
                            break
                    # タイムアウト(2s)
                    elif (time.time() - st) > 2:
                        break
                # タイムアウト(2s)
                if (time.time() - st) > 2:
                    break
#                else:
#                    time.sleep(0.01)

            return bSended
        except:
            pass
