#!/usr/bin/env python
# coding: UTF-8
#
# user_conf.py
#
# スマート電力量メーター　ユーザ設定
#
#

SEM_ROUTEB_ID = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'
SEM_PASSWORD = 'XXXXXXXXXXXX'
SEM_INTERVAL = 3  # 瞬時電力取得間隔[s]
SEM_DURATION = 6  # アクティブスキャンduration (通常は変更の必要なし)

DB_USER = 'dbuser'
DB_PASSWORD = 'dbpassword'
DB_HOST = 'localhost'
DB_DBNAME = 'dbname'
