#!/usr/bin/env python
# coding: UTF-8
#
# sem.py
#
# BルートWi-SUN接続用メインプログラム
#
# sem = Smart Energy Meter
#
# https://github.com/yawatajunk/Wi-SUN_EnergyMeter を参考にリライト
# Copyright(C) 2017 kazuhiloyagi, 2016 pi@blue-black.ink
#

from __future__ import print_function

import datetime
import sys
import threading
import time

from echonetlite_smart_energy_meter import EchonetLiteSmartEnergyMeter
import serial
import user_conf
from wisun_module import WiSunModuleBp
from postgres_sem import PostgresSEM


SERIAL_PORT_DEV = '/dev/ttyUSB0'  # シリアルポートデバイス名(Linux)
# SERIAL_PORT_DEV = 'COM8'          # シリアルポートデバイス名(Windows)  # debug
SERIAL_BAUDRATE = 115200          # シリアル通信速度[bit/s]
SERIAL_TIMEOUT = 1                # シリアル通信のタイムアウト[s]

# sem_infc_dict = {}      # スマートメータのプロパティ値通知（応答要）データ
tid_counter = 0         # TIDカウンタ
pana_ts = 0.0           # PANA認証時のタイムスタンプを記録

bSem_exist = False      # スマートメータ発見フラグ
bPana_done = False      # PANA認証フラグ
bInfc_exist = False     # プロパティ値通知（応答要) \0x74 (INFC) を受信フラグ
db = None               # データベース接続オブジェクト


def pana_connect(wisun, bPana_done, bInfc_exist):
    """プロパティ値通知（応答要）を受信するシーケンス"""

    sem_infc = {}
    if bInfc_exist is False:
        print('bPana_done is: ' + str(bPana_done))  # debug
        # PANA接続完了したら
        if bPana_done:
            st = time.time()  # ループ開始時点の時刻を保存
            while True:
                # ECHONET Lite サービスコードESV コード: プロパティ値通知（応答要) \0x74 (INFC) を受信したら
                # (パターン1)wisun.sem_infc_list にINFC パケットが入っていた場合
                sem_infc = wisun.dequeue_infc()
                if sem_infc:
                    print('[sem_infc]: Successfully done.\n')
                    bInfc_exist = True
                    break

                # (パターン2)INFC パケットを直接シリアルから読み込む場合
                msg = wisun.read()
                if msg:
                    # 1行の受信データを分割してパースする
                    msg_dict = wisun.parse_message(msg)
                    if msg_dict['COMMAND'] == 'ERXUDP':
                        if 'DATA' in msg_dict:
                            if msg_dict['DATA'][0:4] == wisun.EHD:
                                # スマートメーターが自発的に発するプロパティ通知
                                if msg_dict['DATA'][20:22] == wisun.ECV_INF_:
                                    sem_infc = msg_dict

                                    if sem_infc:
                                        print('[sem_infc]: Successfully done.\n')
                                        bInfc_exist = True
                                        break
                                    # 受信パケット読み取り失敗によるタイムアウト(15s)
                                    elif (time.time() - st) > 15:
                                        print('[sem_infc]: Fail to connect.\n')
                                        bInfc_exist = False
                                        break
                                    else:
                                        time.sleep(0.1)

                                if msg_dict['DATA'][20:22] == wisun.ECV_INFC:
                                    sem_infc = msg_dict

                                    if sem_infc:
                                        print('[sem_infc]: Successfully done.\n')
                                        bInfc_exist = True
                                        break
                                    # 受信パケット読み取り失敗によるタイムアウト(15s)
                                    elif (time.time() - st) > 15:
                                        print('[sem_infc]: Fail to connect.\n')
                                        bInfc_exist = False
                                        break
                                    else:
                                        time.sleep(0.1)
                                if msg_dict['DATA'][20:22] == '52':
                                    print('[sem_infc]: The request was denied.\n')
                                    bInfc_exist = False
                                    break
                        # 受信パケット読み取り失敗によるタイムアウト(15s)
                        elif (time.time() - st) > 15:
                            print('[sem_infc]: Fail to connect.\n')
                            bInfc_exist = False
                            break
                        else:
                            time.sleep(0.1)
                    # 受信パケット読み取り失敗によるタイムアウト(15s)
                    elif (time.time() - st) > 15:
                        print('[sem_infc]: Fail to connect.\n')
                        bInfc_exist = False
                        break
                    else:
                        time.sleep(0.1)
                else:
                    time.sleep(0.01)

            return sem_infc



def parse_infc(db, sem_infc_dict):
    """ECHONET Lite サービスコードESV コード: プロパティ値通知（応答要）\0x74 (INFC) フレームの中身を取得して処理する"""

    result = False
    if sem_infc_dict['COMMAND'] == 'ERXUDP':
        # ECHONET Liteフレーム bytes形式列をdict形式に変換する
        parsed_data = EchonetLiteSmartEnergyMeter.parse_frame(sem_infc_dict['DATA'])
        # 動作状態 の応答だった場合
        for ptys in parsed_data['ptys']:
            # print('ptys["edt"]: ' + str(int.from_bytes(ptys['edt'])))
            if ptys['epc'] == b'\xea':
                date_time = EchonetLiteSmartEnergyMeter.parse_datetime(ptys['edt'][0:0+7])
                kilowatt_hour = int.from_bytes(ptys['edt'][7:7+4],  byteorder='big', signed=True)
                print('[Smart Energy Meter]: Recent 30 minutes Amount Energy (normal current) is {0}[kWh] '.format(kilowatt_hour))
                fmt = '%A, %B, %d, %Y, local time %I:%M:%S%p'
                print('at {0}.\n'.format(date_time.strftime(fmt)))
                # データベースへの書込み
                db.insert_ea(kilowatt_hour, date_time)
                result = True
                break
            else:
                print('[ECHONet Frame Error]: Unknown data received.\n')
                result = False

    return result



if __name__ == '__main__':
    """メイン関数"""

    # WiSunModule インスタンスを生成する
    wisun = WiSunModuleBp()
    # UART接続をオープンする
    wisun.uart_open(dev=SERIAL_PORT_DEV, baud=SERIAL_BAUDRATE, timeout=SERIAL_TIMEOUT)
    # WiSunModule スレッドを開始する
    wisun.start()
    print('Wi-SUN reset...\n')

    # Wi-SUN BルートWi-SUN デバイスの初期設定
    # エコーバックを停止する
    wisun.set_echoback_off()
    # RXUDP, ERXTCPのフォーマット設定を16進数ASCII表示に設定する
    # wisun.set_opt(True)
    # Bルート認証パスワード設定
    wisun.set_password(user_conf.SEM_PASSWORD)
    # Bルート認証ID設定
    wisun.set_routeb_id(user_conf.SEM_ROUTEB_ID)

    # Wi-SUN BルートWi-SUN デバイスのネットワーク検出
    channel_list = []

    for i in range(10):
        print('({}/10) Active scan with a duration of {}...\n'.format(i+1, user_conf.SEM_DURATION))
        # アクティブスキャン
        channel_list = wisun.active_scan(user_conf.SEM_DURATION)
        # スキャンしてsemが見つかったら
        if channel_list:
            bSem_exist = True
            break  # スキャンループを抜け出す

    # スキャンに失敗した場合
    if not bSem_exist:
        print('[Error][ActiveScan]: Can not connect to a smart energy meter\n')

    # スキャンに成功した場合
    if bSem_exist:

        ch = channel_list[0]
#        print(ch)
        print('Active scan succeed. Energy Meter: [Ch.0x{:02X}, Addr.{}, PAN.0x{:04X}]\n'.format(ch['Channel'], ch['Addr'], ch['Pan ID']))
        # スキャン結果からChannelを設定する
        wisun.set_channel(ch['Channel'])
#        print('Set channel to 0x{:02X}\n'.format(ch['Channel']))
        # スキャン結果からPan IDを設定する
        wisun.set_pan_id(ch['Pan ID'])
#        print('Set PAN ID to 0x{:04X}\n'.format(ch['Pan ID']))
        # MACアドレス(64bit)をIPV6リンクローカルアドレスに変換する
        ip6 = wisun.get_ip6(ch['Addr'])
        print('IP6 address is \'{}\'\n'.format(ip6))

        # PostgreSQL との接続を確保
        db = PostgresSEM()

        # PANA認証(PaC) 接続完了待ちループ
        while True:
            if bPana_done is False:
                # PANA 接続シーケンスを開始する
                bPana_done = wisun.start_pac(ip6)
                # プロパティ値通知（応答要）を受信するシーケンス
                sem_infc = pana_connect(wisun, bPana_done, bInfc_exist)
                if sem_infc:
                    # ECHONET Lite サービスコードESV コード: プロパティ値通知（応答要）\0x74 (INFC) フレームの中身を取得して処理する
                    b = parse_infc(db, sem_infc)
                    print('parse_infc : ' + str(b))  # debug
                    # PANA認証時のタイムスタンプを保存
                    pana_ts = time.time()
                    bInfc_exist = False
                    break
            else:
                break

    # PANA接続完了したら
    if bPana_done:
        start = time.time()

        # EchonetLiteSmartEnergyMeter インスタンスを生成する
        sem = EchonetLiteSmartEnergyMeter()
        # UDP送信と瞬時電力値データ受信ループ(無限ループ)
        while True:
            try:
                """ 12時間毎にPANA認証を更新するシーケンス """
                # if (time.time() - pana_ts) > (12 * 60 * 60):
                #     # bPana_done = False
                #     print('PANA re-connection...\n')
                #     # PANA 接続シーケンスを再開する
                #     while True:
                #         bPana_done = wisun.restart_pac()
                #         # プロパティ値通知（応答要）を受信するシーケンス
                #         sem_infc = pana_connect(wisun, bPana_done, bInfc_exist)
                #         if sem_infc:
                #             # ECHONET Lite サービスコードESV コード: プロパティ値通知（応答要） \0x74 (INFC) フレームの中身を取得して処理する
                #             b = parse_infc(db, sem_infc)
                #             print('parse_infc : ' + str(b))  # debug
                #             # PANA認証時のタイムスタンプを保存
                #             pana_ts = time.time()
                #             bInfc_exist = False
                #             break

                #         elif bPana_done:
                #             break
                #
                # インターバル待ちをする(強制)
                while True:
                    if (time.time() - start) >= user_conf.SEM_INTERVAL:
                        start = time.time()
                        break
                    else:
                        time.sleep(0.1)

                """プロパティ値読み出し要求を送信し、受信するシーケンス"""
                # print('Sending UDP Packet...\n')
                # # TIDカウントアップ
                # if (tid_counter + 1) < 65536:
                #     tid_counter = tid_counter + 1
                # else:
                #     tid_counter = 0
                # # TIDを ECHONET Liteフレーム dict形式にセットする
                # sem.frame_dict['tid'] = tid_counter.to_bytes(2, byteorder='big')  # int型を 16進数2バイト値に変換する
                # # プロパティ値読み出し要求 (¥xE7:瞬時電力計測値) を ECHONET Liteフレーム dict形式にセットする
                # sem.frame_dict['ptys'].pop(0)
                # sem.frame_dict['ptys'].append(sem.LVSM_EPC_DICT['instant_power'] + b'\x00')
                # # ECHONET Liteフレーム dict形式をbytes形式列に変換する
                # udp_frame = sem.get_serialized_frame()
                # # UDP GETコマンドを送信する
                # res = wisun.udp_send(1, ip6, True, wisun.WISUN_UDP_ECHONET_PORT, udp_frame)
                # # 送信完了
                # if res:
                #     # 受信データ待ちループ
                #     while True:
                #         # データ受信の場合
                #         if wisun.get_queue_size():
                #             # 受信データ取り出し
                #             msg_dict = wisun.dequeue_message()
                #             if msg_dict['COMMAND'] == 'ERXUDP':
                #                 if 'DATA' in msg_dict:
                #                     # ECHONET Liteフレーム bytes形式列をdict形式に変換する
                #                     parsed_data = sem.parse_frame(msg_dict['DATA'])
                #                     # TIDチェック
                #                     if p != tid_counter:
                #                         print('[Error][UDP]: ECHONET Lite TID mismatch\n')
                #                         break    # pass 文は何もしません
                #                     else:
                #                         if parsed_data['esv'] == sem.ESV_DICT['get_res']:
                #                             # 動作状態 の応答だった場合
                #                             if parsed_data['ptys'][0]['epc'] == sem.LVSM_EPC_DICT['operation_status']:
                #                                 if parsed_data['ptys'][0]['edt'] == b'\x30':
                #                                     operation_status = 'ON'
                #                                 elif parsed_data['ptys'][0]['edt'] == b'\x31':
                #                                     operation_status = 'OFF'
                #                                 print('[Smart Energy Meter]: Operation Status is {0}.\n'.format(operation_status))
                #                                 break

                #                             # 瞬時電力計測値 の応答だった場合
                #                             if parsed_data['ptys'][0]['epc'] == sem.LVSM_EPC_DICT['instant_power']:
                #                                 instant_power = int.from_bytes(parsed_data['ptys'][0]['edt'],  byteorder='big', signed=True)
                #                                 print('[Smart Energy Meter]: Instant Power is {0}[W].\n'.format(instant_power))
                #                                 # データベースへの書込み
                #                                 db.insert_e7(instant_power)
                #                                 break
                #                         elif parsed_data['esv'] == sem.ESV_DICT['get_sna']:
                #                             print('[Error][UDP]: The request was denied.\n')
                #                             if (time.time() - start) > 20:    # タイムアウト(20s)
                #                                 print('[Error][UDP]: Time out.\n')
                #                                 break
                #                             else:
                #                                 time.sleep(0.01)
                #                         else:
                #                             print('[Error][UDP]: Unknown data received.\n')
                #                             if (time.time() - start) > 20:    # タイムアウト(20s)
                #                                 print('[Error][UDP]: Time out.\n')
                #                                 break
                #                             else:
                #                                 time.sleep(0.01)
                #                 else:
                #                     print('[Error][UDP]: Unknown data received.\n')
                #                     if (time.time() - start) > 20:    # タイムアウト(20s)
                #                         print('[Error][UDP]: Time out.\n')
                #                         break
                #                     else:
                #                         time.sleep(0.01)
                #             else:
                #                 print('[Error][UDP]: Unknown data received.\n')
                #                 if (time.time() - start) > 20:    # タイムアウト(20s)
                #                     print('[Error][UDP]: Time out.\n')
                #                     break
                #                 else:
                #                     time.sleep(0.01)

                #         else:   # データ未受信の場合
                #             if (time.time() - start) > 20:    # タイムアウト(20s)
                #                 print('[Error][UDP]: Time out.\n')
                #                 break
                #             else:
                #                 time.sleep(0.01)
                # else:
                #     print('[ERROR][UDP] Could not send a packet...\n')

                """プロパティ値通知（応答要）を受信するシーケンス"""
                print('Reading INFC received packet...\n')
                sem_infc = pana_connect(wisun, bPana_done, bInfc_exist)
                if sem_infc:
                    # ECHONET Lite サービスコードESV コード: プロパティ値通知（応答要） \0x74 (INFC) フレームの中身を取得して処理する
                    b = parse_infc(db, sem_infc)
                    print('parse_infc : ' + str(b))  # debug
                    # PANA認証時のタイムスタンプを保存
                    # pana_ts = time.time()
                    bInfc_exist = False

            # 受信ループをCtrl+cで終了したとき
            except KeyboardInterrupt:
                break

    else:
        print('[Error][PANA]: Can not connect with a smart energy meter.\n')

    # 終了処理
    print('\nWi-SUN reset...\n')
    # PostgreSQL 接続をクローズする
    db.terminate()
    # run()の停止
    wisun.terminate()
    # UART接続をクローズする
    wisun.uart_close()

    print('Bye.\n')
    sys.exit(0)
